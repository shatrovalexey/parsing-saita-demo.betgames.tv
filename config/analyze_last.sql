/*
Сумма красных шаров в одном тираже могут быть либо "меньше" либо "больше"   13,5
https://app.asana.com/0/1113894721107509/1114654566639219/f
*/
INSERT INTO
	`draw_info`( `comment`, `array` , `draw_id` )
SELECT
	'Сумма красных шаров в одном тираже могут быть либо "меньше" либо "больше" 13,5' AS `comment` ,
	CASE
		WHEN ( `t1`.`sum` < 13.5 ) THEN '<'
		WHEN ( `t1`.`sum` > 13.5 ) THEN '>'
		ELSE '?'
	END AS `array` ,
	`t1`.`draw_id`
FROM (
	SELECT
		`vdbl1`.`draw_id` ,
		sum( CASE
			WHEN ( `vdbl1`.`color` = 'red' ) THEN `vdbl1`.`value`
			ELSE 0
		END ) AS `sum`
	FROM
		`v_draw_ball_last` AS `vdbl1`
) AS `t1` ;
/*
Сумма синих шаров в одном тираже могут быть либо "больше"  либо "меньше" 12,5
https://app.asana.com/0/1113894721107509/1114654566639220/f
*/
INSERT INTO
	`draw_info`( `comment` , `array` , `draw_id` )
SELECT
	'Сумма синих шаров в одном тираже могут быть либо "больше"  либо "меньше" 12,5' AS `comment` ,
	CASE
		WHEN ( `t1`.`sum` < 12.5 ) THEN '<'
		WHEN ( `t1`.`sum` > 12.5 ) THEN '>'
		ELSE '?'
	END AS `array` ,
	`t1`.`draw_id`
FROM (
	SELECT
		`vdbl1`.`draw_id` ,
		sum( CASE
			WHEN ( `vdbl1`.`color` = 'blue' ) THEN `vdbl1`.`value`
			ELSE 0
		END ) AS `sum`
	FROM
		`v_draw_ball_last` AS `vdbl1`
) AS `t1` ;
/*
общая сумма всех чисел в одном тираже может быть "меньше" либо "больше" 26,5
https://app.asana.com/0/1113894721107509/1114654566639221/f
*/
INSERT INTO
	`draw_info`( `comment` , `array` , `draw_id` )
SELECT
	'Общая сумма всех чисел в одном тираже может быть "меньше" либо "больше" 26,5' AS `comment` ,
	CASE
		WHEN ( sum( `vdbl1`.`value` ) < 26.5 ) THEN '<'
		WHEN ( sum( `vdbl1`.`value` ) > 26.5 ) THEN '>'
		ELSE '?'
	END AS `array` ,
	`vdbl1`.`draw_id`
FROM
	`v_draw_ball_last` AS `vdbl1`
GROUP BY
	`vdbl1`.`draw_id` ;

/*
сумма чисел в A-B-C-зоне в одном тираже может быть больше или меньше 9,5
https://app.asana.com/0/1113894721107509/1114654566639222/f
https://app.asana.com/0/1113894721107509/1114654566639223/f
https://app.asana.com/0/1113894721107509/1114654566639224/f
*/
INSERT INTO
	`draw_info`( `comment` , `array` , `section_pos` , `draw_id` )
SELECT
	'Сумма чисел в зоне в одном тираже может быть больше или меньше 9,5' AS `comment` ,
	CASE
		WHEN ( sum( `vdbl1`.`value` ) < 9.5 ) THEN '<'
		WHEN ( sum( `vdbl1`.`value` ) > 9.5 ) THEN '>'
		ELSE '?'
	END AS `array` ,
	`vdbl1`.`section_pos` ,
	`vdbl1`.`draw_id`
FROM
	`v_draw_ball_last` AS `vdbl1`
GROUP BY
	`vdbl1`.`section_pos` ,
	`vdbl1`.`draw_id` ;

/*
Количество чётных чисел в одном тираже может быть больше или меньше 2,5
https://app.asana.com/0/1113894721107509/1114654566639226/f
*/
INSERT INTO
	`draw_info`( `comment` , `array` , `draw_id` )
SELECT
	'Количество чётных чисел в одном тираже может быть больше или меньше 2,5' AS `comment` ,
	CASE
		WHEN ( sum( `vdbl1`.`even` = 0 ) < 2.5 ) THEN '<'
		WHEN ( sum( `vdbl1`.`even` = 0 ) > 2.5 ) THEN '>'
		ELSE '?'
	END AS `array` ,
	`vdbl1`.`draw_id`
FROM
	`v_draw_ball_last` AS `vdbl1`
GROUP BY
	`vdbl1`.`draw_id` ;

/*
Количество нечётных чисел в одном тираже может быть больше или меньше 2,5
https://app.asana.com/0/1113894721107509/1114654566639227/f
*/
INSERT INTO
	`draw_info`( `comment` , `array` , `draw_id` )
SELECT
	'Количество нечётных чисел в одном тираже может быть больше или меньше 2,5' AS `comment` ,
	CASE
		WHEN ( sum( `vdbl1`.`even` = 1 ) < 2.5 ) THEN '<'
		WHEN ( sum( `vdbl1`.`even` = 1 ) > 2.5 ) THEN '>'
		ELSE '?'
	END AS `array` ,
	`vdbl1`.`draw_id`
FROM
	`v_draw_ball_last` AS `vdbl1`
GROUP BY
	`vdbl1`.`draw_id` ;

/*
сумма чисел в А-B-C-зоне в одном тираже может быть чётной или нечётной
https://app.asana.com/0/1113894721107509/1114654566639228/f
https://app.asana.com/0/1113894721107509/1114654566639229/f
https://app.asana.com/0/1113894721107509/1114654566639230/f
*/
INSERT INTO
	`draw_info`( `comment` , `array` , `section_pos` , `draw_id` )
SELECT
	'Сумма чисел в зоне в одном тираже может быть чётной или нечётной' AS `comment` ,
	CASE ( sum( `vdbl1`.`value` ) % 2 )
		WHEN 0 THEN '<'
		WHEN 1 THEN '>'
		ELSE '?'
	END AS `array` ,
	`vdbl1`.`section_pos` ,
	`vdbl1`.`draw_id`
FROM
	`v_draw_ball_last` AS `vdbl1`
GROUP BY
	`vdbl1`.`section_pos` ,
	`vdbl1`.`draw_id` ;