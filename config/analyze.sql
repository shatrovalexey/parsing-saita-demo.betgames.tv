DROP VIEW IF EXISTS `v_draw_last` ;
DROP VIEW IF EXISTS `v_draw_last8` ;
DROP VIEW IF EXISTS `v_draw_ball_last` ;
DROP VIEW IF EXISTS `v_draw_analyze_last8` ;
DROP VIEW IF EXISTS `v_draw_analyze_last8_sign` ;
DROP VIEW IF EXISTS `v_draw_analyze_last8_result` ;

CREATE VIEW `v_draw_analyze_last8_result` AS
SELECT
	`b1`.`value` ,
	`b1`.`color` ,
	`d1`.`id` AS `draw_id` ,
	`d1`.`created` ,
	`s1`.`id` AS `sector_id`
FROM
	(
		SELECT
			`d1`.*
		FROM
			`draw` AS `d1`
		ORDER BY
			`d1`.`created` DESC
		LIMIT 8
	) AS `d1`

	INNER JOIN `sector` AS `s1` ON
	( `d1`.`id` = `s1`.`draw_id` )

	INNER JOIN `ball` AS `b1` ON
	( `b1`.`sector_id` = `s1`.`id` )
ORDER BY
	`d1`.`created` DESC ,
	`s1`.`pos` ASC ,
	`b1`.`pos` ASC ;

CREATE VIEW `v_draw_last` AS
SELECT
	`d1`.`id` AS `draw_id`
FROM
	`draw` AS `d1`
ORDER BY
	`d1`.`created` DESC
LIMIT 1 ;

CREATE VIEW `v_draw_last8` AS
SELECT
	`d1`.`id` AS `draw_id`
FROM
	`draw` AS `d1`
ORDER BY
	`d1`.`created` DESC
LIMIT 8 ;

CREATE VIEW `v_draw_ball_last` AS
SELECT
	`vdl1`.`draw_id` ,
	`s1`.`id` AS `sector_id` ,
	`b1`.`id` AS `ball_id` ,
	`b1`.`color` ,
	`b1`.`value` ,
	`b1`.`value` % 2 AS `even` ,
	`s1`.`pos` AS `section_pos`
FROM
	`v_draw_last` AS `vdl1`

	INNER JOIN `sector` AS `s1` ON
	( `vdl1`.`draw_id` = `s1`.`draw_id` )

	INNER JOIN `ball` AS `b1` ON
	( `s1`.`id` = `b1`.`sector_id` ) ;

CREATE VIEW `v_draw_analyze_last8_sign` AS
SELECT
	`t1`.`comment` ,
	group_concat( `t1`.`array` , '' ) AS `array` ,
	group_concat( `t1`.`draw_id` ) AS `alert_id` ,
	group_concat( `t1`.`created` ) AS `created`
FROM
	(
	SELECT
		`d1`.`created` ,
		`di1`.`draw_id` ,
		CASE
			WHEN `di1`.`section_pos` IS null THEN
				`di1`.`comment`
			ELSE
				`di1`.`comment` ||
				CASE
					WHEN `di1`.`section_pos` THEN '. Сектор ' || `di1`.`section_pos`
					ELSE ''
				END
		END AS `comment` ,
		CASE `di1`.`array`
			WHEN '0' THEN '<'
			WHEN '1' THEN '>'
			ELSE `di1`.`array`
		END AS `array`
	FROM
		`draw` AS `d1`

		INNER JOIN `v_draw_last8` AS `vdl81` ON
		( `d1`.`id` = `vdl81`.`draw_id` )

		LEFT OUTER JOIN `draw_info` AS `di1` ON
		( `d1`.`id` = `di1`.`draw_id` )
	WHERE
		( `di1`.`array` IN ( '<' , '>' , '0' , '1' ) )
	GROUP BY
		`d1`.`created` ,
		`di1`.`draw_id` ,
		`di1`.`comment` ,
		`di1`.`section_pos`
	ORDER BY
		`d1`.`created` ASC ,
		`di1`.`draw_id` ,
		`di1`.`comment` ,
		`di1`.`section_pos`
	) AS `t1`
GROUP BY
	1
HAVING
	( group_concat( `t1`.`array` , '' ) IN ( '<><><><>' , '><><><><' , '>>>>>><<' , '<<<<<<>>' , '<<<<<<<<' , '>>>>>>>>' ) ) ;

CREATE VIEW `v_draw_analyze_last8` AS
SELECT
	`di1`.`array` ,
	`di1`.`section_pos` ,
	`di1`.`comment` ,
	group_concat( `vdl81`.`draw_id` ) AS `alert_id`
FROM
	`v_draw_last8` AS `vdl81`

	LEFT OUTER JOIN `draw_info` AS `di1` ON
	( `vdl81`.`draw_id` = `di1`.`draw_id` )
WHERE
	( `di1`.`array` IN ( '<' , '>' , '0' , '1' ) )
GROUP BY
	`di1`.`array` ,
	`di1`.`section_pos` ,
	`di1`.`comment`
HAVING
	( count( DISTINCT `di1`.`draw_id` ) = 8 )
UNION ALL
SELECT
	`vdals1`.`array` ,
	null AS `section_pos` ,
	`vdals1`.`comment` ,
	`vdals1`.`alert_id`
FROM
	`v_draw_analyze_last8_sign` AS `vdals1` ;