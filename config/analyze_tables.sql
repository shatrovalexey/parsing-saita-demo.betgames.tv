DROP TABLE IF EXISTS `draw_info` ;
DROP TABLE IF EXISTS `alert` ;

CREATE TABLE `draw_info`(
	`id` CHARACTER( 40 ) NOT null DEFAULT ( get_id( ) ) ,
	`draw_id` CHARACTER( 40 ) NOT null ,
	`array` CHARACTER( 1 ) ,
	`section_pos` INTEGER NOT null DEFAULT ( 0 ) ,
	`comment` VARYING CHARACTER( 300 ) NOT null ,

	PRIMARY KEY( `id` )
) ;
CREATE TABLE `alert`(
	`id` CHARACTER( 40 ) NOT null ,
	`message` TEXT NOT null ,
	`created` DATETIME NOT null DEFAULT ( datetime( 'now' , 'localtime' ) ) ,

	PRIMARY KEY( `id` )
) ;
CREATE UNIQUE INDEX `idx_draw_info_draw_id_comment` ON `draw_info` (
	`draw_id` ,
	`array` ,
	`section_pos` ,
	`comment`
) ;