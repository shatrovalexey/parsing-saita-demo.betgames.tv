DROP TABLE IF EXISTS `draw` ;
DROP TABLE IF EXISTS `sector` ;
DROP TABLE IF EXISTS `ball` ;
CREATE TABLE `draw`(
	`id` CHARACTER( 40 ) NOT null ,
	`created` DATETIME NOT null DEFAULT ( datetime( 'now' , 'localtime' ) ) ,
	`hash` CHARACTER( 40 ) NOT null ,

	PRIMARY KEY( `id` )
) ;
CREATE TABLE `sector`(
	`id` CHARACTER( 40 ) NOT null ,
	`draw_id` CHARACTER( 40 ) NOT null ,
	`pos` INTEGER NOT null ,

	PRIMARY KEY( `id` )
) ;
CREATE TABLE `ball`(
	`id` CHARACTER( 40 ) NOT null ,
	`sector_id` CHARACTER( 40 ) NOT null ,
	`pos` INTEGER NOT null ,
	`value` INTEGER NOT null ,
	`color` VARYING CHARACTER( 10 ) NOT null ,

	PRIMARY KEY( `id` )
) ;
CREATE UNIQUE INDEX `idx_draw_pos_created_id` ON `draw` (
	`created` DESC ,
	`id`
) ;
CREATE INDEX `idx_draw_hash` ON `draw` (
	`hash`
) ;
CREATE INDEX `idx_sector_draw_id` ON `sector` (
	`draw_id`
) ;
CREATE INDEX `idx_ball_sector_id` ON `ball` (
	`sector_id`
) ;