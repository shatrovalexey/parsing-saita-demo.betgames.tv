import time
import sqlite3
import json
import hashlib
import uuid
import atexit
import signal
import sys
import os
from setproctitle import setproctitle
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from lxml import html
import smtplib
from email.message import EmailMessage

try : import win32api
except : pass

"""
Загрузка настроек
"""
config = json.loads( open( 'config/config.json' ).read( ) )

"""
Проверка существования процесса
"""

proc_name = '%s-%s' % ( sys.argv[ 0 ] , config[ 'url' ] )

for line in os.popen( "ps auxf | grep '%s' | wc -l" % proc_name ).read( ) :
	assert int( line ) == 2

	break
else : exit( 0 )

setproctitle( proc_name )

"""
Процедура при завершении программы
"""
def done( * args , ** kwargs ) :
	try : wdh.quit( )
	except : pass

	try : dbh.close( )
	except : pass

	try : conn.commit( )
	except : pass

	exit( 0 )

"""
Использование процедуры завершения программы
"""
signal.signal( signal.SIGINT , done )
signal.signal( signal.SIGTERM , done )
signal.signal( signal.SIGILL , done )
atexit.register( done )


"""
Генерация оповещения
"""

def showMessage( message ) :
	try : win32api.MessageBox( 0 , message , config[ 'alert' ][ 'title' ] , 0x00001000 )
	except : print( message )

def sendMail( message ) :
	email_config = get_email( )

	for mail_address in email_config[ 'to' ] :
		msg = EmailMessage( )
		msg.set_content( message )
		msg[ 'From' ] = email_config[ 'from' ]
		msg[ 'To' ] = mail_address
		msg[ 'Subject' ] = config[ 'alert' ][ 'title' ]

		sh = smtplib.SMTP( email_config[ 'host' ] )
		sh.send_message( msg )
		sh.quit( )

		print( mail_address )

def alert( messages ) :
	if not messages : return

	message = "\n".join( messages )

	sendMail( message )
	showMessage( message )

def db_script( * args ) :
	for file_name in args :
		dbh.executescript( open( file_name ).read( ) )

"""
Генерация идентификатора
"""
def get_id( str = None ) :
	if str is None : str = uuid.uuid4( ).hex

	return hashlib.sha1( str.encode( ) ).hexdigest( )

def get_hash( * args ) :
	result = [ ]

	for arg in args : result.append( get_id( arg ) )

	return get_id( "\n".join( result ) )

"""
Загрузка списка e-mail
"""
def get_email( ) :
	return json.loads( open( config[ 'mail' ] ).read( ) )

"""
Создание подключения к БД
"""
conn = sqlite3.connect( config[ 'db' ][ 'path' ] )
conn.create_function( 'get_id' , 0 , get_id )
conn.create_function( 'sha1' , 1 , get_id )
conn.create_function( 'strrev' , 1 , lambda s : s[ :: -1 ] )
dbh = conn.cursor( )

"""
Создание БД
"""

try : assert sys.argv[ 1 ] == 'no-database'
except : db_script( 'config/collect_tables.sql' , 'config/analyze_tables.sql' )

"""
Создание объекта webdriver
"""
wdh_options = Options( )
wdh_options.add_experimental_option( 'prefs' , config[ 'webdriver' ][ 'prefs' ] )
for option in config[ 'webdriver' ][ 'options' ] : wdh_options.add_argument( option )

def get_webdriver( ) :
	while True :
		try : wdh.quit( )
		except : pass
		try :
			wdh = webdriver.Chrome( executable_path = config[ 'webdriver' ][ 'path' ] , chrome_options = wdh_options )
			wdh.get( config[ 'url' ] )

			iframe = wdh.find_element_by_id( 'betgames_iframe_1' )
			wdh.switch_to_frame( iframe )

			for game_node in wdh.find_elements_by_css_selector( '.tabs-bar-item' ) :
				for game_title_node in game_node.find_elements_by_css_selector( '.game-title' ) :
					if game_title_node.text != 'LUCKY 6' : continue

					game_title_node.click( )
					break
				else : continue

				return wdh
		except : pass

wdh = get_webdriver( )

while True :
	time.sleep( 1 )

	try :
		try : wdh.find_elements_by_css_selector( '.game-result' )
		except : continue

		wait_results = False
		game_results = [ ]
		last_results = None

		for ( last_result_hash , ) in dbh.execute( '''
SELECT
	`d1`.`hash`
FROM
	`draw` AS `d1`
ORDER BY
	`d1`.`created` DESC
LIMIT 1 ;
		''' ) : last_results = last_result_hash

		for game_result_node in wdh.find_elements_by_css_selector( '.game-result' ) :
			game_result = [ ]
			sector_nodes = [ ]

			while True :
				try : sector_nodes = game_result_node.find_elements_by_css_selector( '.sector' )
				except : wait_results = True

				break

			if wait_results : break

			for sector_node in sector_nodes :
				sector = [ ]
				ball_nodes = [ ]

				while True :
					try : ball_nodes = sector_node.find_elements_by_css_selector( '.ball-item' )
					except : wait_results = True

					break

				for ball_node in ball_nodes :
					try :
						sector.append( {
							'value' : int( ball_node.text ) ,
							'color' : ball_node.get_attribute( 'class' )[ 10 : ]
						} )
					except :
						wait_results = True
						break

				if wait_results : break
				game_result.append( sector )

			if wait_results : break
			game_results.append( game_result )

		if wait_results : continue

		current_results = get_id( json.dumps( game_results ) )
		if last_results == current_results : continue

		try : game_result[ 0 ][ 0 ]
		except : continue

		last_results = current_results

		for game_result in game_results :
			draw_id = get_id( )
			sector_pos = 0

			dbh.execute( '''
INSERT INTO `draw`( `id` , `hash` ) VALUES ( ? , ? ) ;
			''' , ( draw_id , last_results , ) )

			for sector in game_result :
				sector_id = get_id( )
				sector_pos += 1
				ball_pos = 0

				dbh.execute( '''
INSERT INTO `sector`( `id` , `draw_id` , `pos` ) VALUES( ? , ? , ? ) ;
				''' , ( sector_id , draw_id , sector_pos , ) )

				for ball in sector :
					ball_id = get_id( )
					ball_pos += 1

					dbh.execute( '''
INSERT INTO `ball`( `id` , `sector_id` , `pos` , `value` , `color` ) VALUES( ? , ? , ? , ? , ? ) ;
					''' , ( ball_id , sector_id , ball_pos , ball[ 'value' ] , ball[ 'color' ] , ) )
			conn.commit( )
			break

		db_script( 'config/analyze.sql' , 'config/analyze_last.sql' )
		conn.commit( )

		messages = [ ]
		dbh2 = conn.cursor( )

		for ( alert_id , comment , array , section_pos , ) in dbh2.execute( '''
SELECT
	`vdal1`.`alert_id` ,
	`vdal1`.`comment` ,
	`vdal1`.`array` ,
	`vdal1`.`section_pos`
FROM
	`v_draw_analyze_last8` AS `vdal1`

	LEFT OUTER JOIN `alert` AS `a1` ON
	( `vdal1`.`alert_id` = `a1`.`id` )
WHERE
	( `a1`.`id` IS null ) ;
		''' ) :
			message = '%s; совпало направление %s' % ( comment , array )

			if section_pos and ( section_pos > 0 ) : message += '; сектор %d' % section_pos

			draw_id_last = None
			draw_table = [ ]

			for ( draw_row , ) in dbh2.execute( '''
SELECT
	`t1`.`created` || '	' || group_concat( `t1`.`sector` , '	' ) AS `row`
FROM
	(
		SELECT
			`dalr1`.`draw_id` ,
			`dalr1`.`sector_id` ,
			`dalr1`.`created` ,
			'(' || group_concat( `dalr1`.`value` || '(' || `dalr1`.`color` || ')' , '|' ) || ')' AS `sector`
		FROM
			`v_draw_analyze_last8_result` AS `dalr1`
		GROUP BY
			`dalr1`.`draw_id` ,
			`dalr1`.`sector_id` ,
			`dalr1`.`created`
		ORDER BY
			`dalr1`.`created` DESC
	) AS `t1`
GROUP BY
	`t1`.`draw_id`
ORDER BY
	`t1`.`created` ASC ;
			''' ) : draw_table.append( draw_row )

			message += "\n" + "\n".join( draw_table )
			messages.append( message )

			dbh.execute( '''
INSERT INTO `alert`( `id` , `message` ) VALUES( sha1( ? ) , ? ) ;
			''' , ( alert_id , message , ) )

		alert( messages )
	except Exception as exception :
		raise exception
		print( exception )

		wdh = get_webdriver( )